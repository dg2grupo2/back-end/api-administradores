FROM openjdk:11
LABEL maintainer "Grupo 2 - desafio final <grupo02.ilab@gmail.com>"

ARG JAR_FILE=target/*.jar

ARG NEW_RELIC_KEY

COPY ${JAR_FILE} app.jar

RUN mkdir -p /newrelic

ADD ./newrelic/newrelic.jar /newrelic/newrelic.jar
ADD ./newrelic/newrelic.yml /newrelic/newrelic.yml

ENV NEW_RELIC_APP_NAME="API_ADMINISTRADORES"
ENV NEW_RELIC_LICENSE_KEY=$NEW_RELIC_KEY
ENV NEW_RELIC_LOG_FILE_NAME=STDOUT

EXPOSE 8089

ENTRYPOINT ["java", "-jar", "-javaagent:/newrelic/newrelic.jar", "app.jar"]
