<p align="center" style="display: flex; align-items: flex-start; justify-content: center;">
<img alt="logo" title="#logo" src="src/assets/logo.png">
</p>

<h1 align="center">
     Desafio-iFood-Back - Equipe 2
</h1>

![Badge em Desenvolvimento](http://img.shields.io/static/v1?label=STATUS&message=FINALIZADO&color=GREEN&style=for-the-badge)


Tópicos
=================
<!--ts-->
   * [Sobre o projeto](#-Sobre-o-projeto)
   * [Informações Gerais](#-Informações-gerais)
   * [API endpoints](#-API-endpoints)
      * [Login de administradores](#-login-de-administradores)
      * [Cadastro de administradores](#-cadastro-de-administradores)
      * [Pesquisar usuario por email](#-Pesquisar-usuario-por-email)
   * [Pré-requisitos](#-Pré-requisitos)
   * [Editar a aplicação ou rodar localmente](#-editar-a-aplicação-ou-rodar-localmente)
   * [Tecnologias](#-tecnologias)
   * [Time de desenvolvimento](#-time-de-desenvolvimento)

---

   ## 💻 Sobre o projeto
Esse projeto tem por objetivo de cadastrar administradores para gerenciar pedidos e usuarios do sistema.

Em resumo, o processo se dará através:

- administrador faz seu cadastro no sistema (estilo web) e faz seu login;
- se conecta no sistema com email e senha cadastrado com validação token jwt;
- cada token é instranferivel e uso único.
- ao se conectar irá lista todos os administradores cadastrados;
- terá a opção de buscar um único administrador através do email cadastrado no BD;
---

## Link complemetares

link para o repositório da API de pedidos no back: [API de pedidos](https://gitlab.com/dg2grupo2/back-end/api-pedidos)

link para o repositório da API de usuarios no back: [API de ususarios](https://gitlab.com/dg2grupo2/back-end/api-ususarios)

link para o repositório do front: [front](https://gitlab.com/dg2grupo2/front-end/app)

link para slide explicativo e esquema do projeto: [Slide](https://docs.google.com/presentation/d/1E25l4NVkKBd4txFzbn1UeNnJnRi1W_Wq-D-KL3r9njY/edit#slide=id.g136204565f9_0_377)

---

## ⚙️ Informações gerais
O sistema guardará estas informações para conexões com administradores através das ferramentas da AWS, sendo a persistência dos dados feito num SGBD MySQL na mesma.

O que foi implementado durante o projeto:
1) endpoint login de administradores (email e senha);
2) endpoint cadastro de administradores (senha, email, nome);
3) endpoint Pesquisar administrador por email, sem acesso para usuario final, apenas programadores e serve para verificar se já existe o emailk cadastrado;

# API endpoints

## Login de administradores

#### `POST` (/login)
```
{
email:email,
senha: senha
}
```
#### Retorno: 
```
200 - TOKEN administrador
```
#### ou
```
Mensagem de erro: 400 - O campo de e-mail e senha são obrigatórios!
```
#### ou
```
Mensagem de erro: 401 - Login e/ou senha inválidos
```
#### ou
```
Mensagem de exceção: 500
```

## Cadastro de administradores

#### `POST` (/cadastrar)
```
{
senha: senha,
email:email,
nome:nome
}
```
#### Retorno:
```
Mensagem de sucesso: 201 - Novo administrador cadastrado com sucesso!
```
#### ou
```
Mensagem de erro: 400 - O campo de e-mail, nome e senha são obrigatórios.
```
#### ou
```
Mensagem de exceção: 500
```

## Pesquisar administrador por email

#### `GET` (/email)
```
{
email:email
}
```
#### Retorno:
```
Mensagem status de sucesso: 200
```
#### ou
```
Mensagem de erro: Usuário não encontrado.
```
#### ou
```
Mensagem de exceção: 500
```

---

## Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git][git], [Java 17][Java17] com [Spring Boot][SpringBoot].

---

## Editar a aplicação ou rodar localmente

```bash

# Clone este repositório em sua máquina  
$ git clone git@gitlab.com:dg2grupo2/back-end/api-administradores.git

```
---

## 🛠 Tecnologias

As seguintes linguagens/tecnologias foram usadas na construção do projeto:

- [Java 11][Java11]
- [Spring Boot][SpringBoot]
- [Kubernetes (K8s)][k8s]
- [Docker][docker]
- [Git][git]
- [AWS][aws]
   - [SQS][sqs]
   - [SES][ses]
   - [New Relic][NewRelic]

---

## 🛠 Ferramentas

- [JIRA][jira]
- [Draw.io][draw.io]

---

## 🦸 Time de desenvolvimento

⚙️**Carolina Marques** - [Gitlab](https://gitlab.com/carumarques) [Linkedin](https://www.linkedin.com/in/caru-marques/)
⚙️**Silvano Araújo** - [Gitlab](https://gitlab.com/silvanoeng) [Linkedin](https://www.linkedin.com/in/silvano-araujo/)
⚙️**Jean Pierre** - [Gitlab](https://gitlab.com/JeanSisse) [Linkedin](https://www.linkedin.com/in/jeanpierresisse/)
⚙️**Eduardo Gomes** - [Gitlab](https://gitlab.com/eduardo377) [Linkedin](https://www.linkedin.com/in/eduardogomes377/)
⚙️**Ana Beatriz Nunes** - [Gitlab](https://gitlab.com/ananuness) [Linkedin](https://www.linkedin.com/in/ana-beatriz-nunes/)

---

[MySQL]:  https://www.mysql.com/
[SpringBoot]: https://spring.io/projects/spring-boot
[Java11]: https://www.oracle.com/br/java/technologies/javase/jdk11-archive-downloads.html
[jira]: https://dg2grupo2.atlassian.net/jira/software/projects/DG2GRUP/boards/1
[aws]:https://aws.amazon.com/pt/?nc2=h_lg
[k8s]:https://kubernetes.io/pt-br/
[docker]:https://docs.docker.com/
[git]:https://git-scm.com
[sqs]:https://aws.amazon.com/pt/sqs/
[ses]:https://aws.amazon.com/pt/ses/
[NewRelic]:https://aws.amazon.com/pt/quickstart/architecture/new-relic-infrastructure/
[draw.io]:https://app.diagrams.net/
