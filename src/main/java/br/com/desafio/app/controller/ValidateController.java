package br.com.desafio.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.app.security.TokenUtil;
import br.com.desafio.app.services.IAdministradorService;

@RestController
@CrossOrigin("*")
public class ValidateController {

	private IAdministradorService admService;
	
	@Autowired
	public ValidateController(IAdministradorService admService) {
		this.admService = admService;
	}


	@PostMapping("/validate")
	public ResponseEntity<?> validate(@RequestHeader("Authorization") String token) {
		
		if (token == null) {
			return ResponseEntity.status(401).body("Usuário não autenticado.");
		}

		try {
			// Criar um modelo de resposta para não usar Authentication
			// retornar apenas o necessário
			TokenUtil tokenUtilClass = new TokenUtil(admService);
			Authentication auth = tokenUtilClass.tokenValidator(token);
			
			return ResponseEntity.ok(auth);
		} catch (Exception ex) {
			ex.printStackTrace();
			return ResponseEntity.status(403).body(ex.getMessage());
		}
	}
}
