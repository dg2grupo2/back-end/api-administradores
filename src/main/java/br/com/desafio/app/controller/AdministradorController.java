package br.com.desafio.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.app.DTO.AdministradorLoginDTO;
import br.com.desafio.app.DTO.AdministradorReqDTO;
import br.com.desafio.app.DTO.AdministradorResDTO;
import br.com.desafio.app.models.Administrador;
import br.com.desafio.app.services.IAdministradorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/administradores")
@CrossOrigin("*")
@Api(value = "Administradores")
public class AdministradorController {

	private IAdministradorService admService;
	
	@Autowired
	public AdministradorController(IAdministradorService admService) {
		this.admService = admService;
	}
	
	@ApiOperation(value = "Retorna Administradores")
	@GetMapping
	public ResponseEntity<?> buscarAdministradores() {
		return ResponseEntity.ok(admService.findAll());
	}
	
	@GetMapping("/{email}")
	public ResponseEntity<?> findByEmail(@PathVariable String email) {
		try {
			System.out.println(email);
			
			Administrador admExist = admService.findByEmail(email.toLowerCase());
			if (admExist != null) {
				return ResponseEntity.ok(new AdministradorResDTO(admExist));
			}
			
			return ResponseEntity.status(404).body("Usuário não encontrado");
		} catch (Exception ex) {
			return ResponseEntity.status(500).body(ex);
		}
	}

	@ApiOperation(value = "Cadastro de administradores")
	@PostMapping("/cadastrar")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid AdministradorReqDTO newAdm) {
		try {
			if (newAdm.getSenha() == null || newAdm.getEmail() == null || newAdm.getNome() == null) {
				return ResponseEntity.status(400).body("O campo de e-mail, nome e senha são obrigatórios!");
			}
			
			AdministradorResDTO admResponse = admService.createAdministrador(newAdm);
			
			if (admResponse != null) {
				return ResponseEntity.status(201).body("Novo administrador cadastrado com sucesso!");
			}
			
			return ResponseEntity.status(400).body("Não foi possível cadastrar o administrador");
		} catch(Exception ex) {
			ex.printStackTrace();
			return ResponseEntity.status(500).body(ex.getMessage());
		}
	}
	
	@ApiOperation(value = "Login de administradores")
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody AdministradorLoginDTO loginData) {
		try {
			if (loginData.getSenha() == null || loginData.getEmail() == null) {
				return ResponseEntity.status(400).body("O campo de e-mail e senha são obrigatórios!");
			}
			
			return ResponseEntity.ok(admService.generateAdmToken(loginData));
		} catch(IllegalArgumentException ex) {
			return ResponseEntity.status(401).body("Login e/ou senha inválidos.");
		} catch(Exception ex) {
			ex.printStackTrace();
			return ResponseEntity.status(500).body(ex.getMessage());
		}
	}
}
