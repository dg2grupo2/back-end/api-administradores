package br.com.desafio.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class ApiAdministradoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiAdministradoresApplication.class, args);
	}

}
