package br.com.desafio.app.services;

import java.util.List;

import br.com.desafio.app.DTO.AdministradorLoginDTO;
import br.com.desafio.app.DTO.AdministradorReqDTO;
import br.com.desafio.app.DTO.AdministradorResDTO;
import br.com.desafio.app.models.Administrador;
import br.com.desafio.app.security.Token;

public interface IAdministradorService {

	AdministradorResDTO createAdministrador(AdministradorReqDTO newAdm);
	Administrador findByEmail(String email);
	AdministradorResDTO findById(Long id);
	Token generateAdmToken(AdministradorLoginDTO admLogin) throws Exception;
	List<AdministradorResDTO> findAll();
}
