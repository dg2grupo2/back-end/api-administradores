package br.com.desafio.app.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.app.DTO.AdministradorLoginDTO;
import br.com.desafio.app.DTO.AdministradorReqDTO;
import br.com.desafio.app.DTO.AdministradorResDTO;
import br.com.desafio.app.models.Administrador;
import br.com.desafio.app.repository.AdministradorRepository;
import br.com.desafio.app.security.SystemCrypto;
import br.com.desafio.app.security.Token;
import br.com.desafio.app.security.TokenUtil;

@Service
public class AdministradorServiceImp implements IAdministradorService {

	private AdministradorRepository repository;
	
	@Autowired
	public AdministradorServiceImp(AdministradorRepository repository) {
		this.repository = repository;
	}

	@Override
	public AdministradorResDTO createAdministrador(AdministradorReqDTO newAdm) {
		try {
			newAdm.setSenha(SystemCrypto.encrypt(newAdm.getSenha()));
			return new AdministradorResDTO(repository.save(new Administrador(newAdm)));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Token generateAdmToken(AdministradorLoginDTO admLogin) throws Exception {
		try {

			Administrador adm = repository.findByEmail(admLogin.getEmail().toLowerCase());
			
			if (adm == null || !adm.getSenha().equals(SystemCrypto.encrypt(admLogin.getSenha()))) {
				throw new IllegalArgumentException("E-mail e/ou senha inválidos.");
			}
			
			return TokenUtil.createToken(adm);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
			throw new IllegalArgumentException();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception("Não foi possível realizar a autenticação!");
		}
	}

	@Override
	public Administrador findByEmail(String email) {
		try {
			Administrador adm = repository.findByEmail(email);
			if (adm == null) {
				System.out.println("não encontrado");
				return null;
			}
			return adm;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@Override
	public List<AdministradorResDTO> findAll() {
		List<AdministradorResDTO> admins = repository.findAll().stream()
				.map(admin -> new AdministradorResDTO(admin)).collect(Collectors.toList());
		return admins;
	}

	@Override
	public AdministradorResDTO findById(Long id) {
		try {
			Administrador adm = repository.findById(id).orElse(null);
			if (adm == null) {
				throw new EntityNotFoundException("Usuário não encontrado.");
			}

			return new AdministradorResDTO(adm);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
