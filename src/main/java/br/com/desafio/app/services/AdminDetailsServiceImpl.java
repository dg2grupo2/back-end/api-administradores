package br.com.desafio.app.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.desafio.app.models.Administrador;
import br.com.desafio.app.repository.AdministradorRepository;
import br.com.desafio.app.security.AdminSpringSecurity;

public class AdminDetailsServiceImpl implements UserDetailsService {

	private AdministradorRepository repository;
	
	@Autowired
	public AdminDetailsServiceImpl(AdministradorRepository repository) {
		this.repository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Administrador adm = repository.findByEmail(email);
		
		if (adm == null) {
			throw new UsernameNotFoundException(email);
		}
		
		return new AdminSpringSecurity(adm.getId(), adm.getEmail(), adm.getSenha(), new ArrayList<>());
	}

}
