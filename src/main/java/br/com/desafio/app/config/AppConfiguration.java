package br.com.desafio.app.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.oas.annotations.EnableOpenApi;

@Configuration
@EnableWebMvc
@EnableOpenApi
@ComponentScan("br.com.desafio.app")
public class AppConfiguration {

}
