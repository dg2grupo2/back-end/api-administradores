package br.com.desafio.app.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SystemSecurityConfiguration {
		
	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
		return http.csrf().disable() //desabilita mecanismos que configura acesso externo da api
				.cors().and().build();
	}
}
