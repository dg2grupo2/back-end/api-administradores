package br.com.desafio.app.security;

import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import br.com.desafio.app.models.Administrador;
import br.com.desafio.app.services.IAdministradorService;

public class TokenUtil {
	
	public static final String PREFIX = "Bearer "; 
	public static final String SECRET_KEY = System.getenv("SECRET_KEY"); 
	public static final Algorithm ALGORITHM = Algorithm.HMAC256(SECRET_KEY);
	public static final String EMISSOR = System.getenv("EMISSOR");
	public static final String HEADER = "Authorization";
	
	private static final int SEGUNDOS = 1000;
	private static final int MINUTOS  = 30 * SEGUNDOS;
	
	private static final long EXPIRATION = 50 * MINUTOS;
	
	
	private IAdministradorService admService;
	
	@Autowired
	public TokenUtil(IAdministradorService admService) {
		this.admService = admService;
	}

	public static Token createToken(Administrador adm) throws Exception {
		
		System.out.println("TEMPO MILLISEGUNDOS: " + System.currentTimeMillis());
		String token = JWT.create().withClaim("userId", adm.getId())
				.withIssuer(EMISSOR)
				.withExpiresAt(new Date(EXPIRATION + System.currentTimeMillis()))
				.sign(ALGORITHM);
		
		return new Token(PREFIX + token, adm.getId());
	}
	
	public Authentication tokenValidator(String token) {

		String jwtToken = null;
		
		if (token == null || !token.startsWith(PREFIX)) {
			System.err.println("Token não contém " + PREFIX);
			throw new JWTVerificationException("Token inválido.");
		}
		
		jwtToken = token.replace(PREFIX, "");
	
		try {
			JWTVerifier verifier = JWT.require(ALGORITHM)
					.withIssuer(EMISSOR)
					.build();

			DecodedJWT decodedJwt = verifier.verify(jwtToken);
			Claim userId = decodedJwt.getClaim("userId");
			Claim exp = decodedJwt.getClaim("exp");
			
			TokenIdUtil admId = userId.as(TokenIdUtil.class);

			if(!isAccountExist(admId.getId())) {
				throw new JWTVerificationException("Usuário inválido.");
			}
			
			if (isTokentExpired(exp.asDate())) {
				throw new JWTVerificationException("Seu token expirou, faça login novamente!");
			}

			return new UsernamePasswordAuthenticationToken(userId, null, Collections.emptyList());
		} catch (TokenExpiredException ex) {
			throw new JWTVerificationException(ex.getMessage());
		} catch (IllegalArgumentException ex) {
			throw new JWTVerificationException(ex.getMessage());
		}
	}

	public boolean isTokentExpired(Date date) {
		return date.before(new Date());
	}
	
	public boolean isAccountExist(Long id) {
		return admService.findById(id) == null ? false : true;
	}

}
