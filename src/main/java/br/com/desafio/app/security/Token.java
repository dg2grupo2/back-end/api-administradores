package br.com.desafio.app.security;

public class Token {
	private String token;
	private Long id;

	public Token(String token, Long  id) {
		this.token = token;
		this.id 	 = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getToken() {
		return token;
	}
}
