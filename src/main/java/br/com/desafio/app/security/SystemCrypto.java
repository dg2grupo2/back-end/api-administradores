package br.com.desafio.app.security;

import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class SystemCrypto {

	private static final String SECRET_KEY = "123456789 123456789 123456789 12";
	private static final String SALT = "123456789098765432";
	
	public static String encrypt(String originalPassword) throws Exception{
		try {
		
		IvParameterSpec ivspec = generateIv();
    SecretKeySpec secretKeySpec = generateKeySpec();
		
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);
    
    return Base64.getEncoder()
        .encodeToString(cipher.doFinal(originalPassword.getBytes(StandardCharsets.UTF_8)));
		} catch (Exception ex) {
			System.err.println("Erro ao encriptar a senha" + ex.toString());
			return null;
		}
	}
	
	public static String decrypt(String encryptedPassword) {
		try {
			IvParameterSpec ivspec = generateIv();
			SecretKeySpec secretKeySpec = generateKeySpec();

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivspec);
			
			return new String(cipher.doFinal(Base64.getDecoder().decode(encryptedPassword)));
		} catch (Exception ex) {
			System.err.println("Erro ao decriptar a senha" + ex.toString());
			return null;
		}
	}

	public static SecretKeySpec generateKeySpec() throws Exception {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
    KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(StandardCharsets.UTF_8), 65536, 256);
    SecretKey secretKey = factory.generateSecret(spec);
    return new SecretKeySpec(secretKey.getEncoded(), "AES");
	}
	
	public static IvParameterSpec generateIv() {
		byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    return new IvParameterSpec(iv);
	}
}
