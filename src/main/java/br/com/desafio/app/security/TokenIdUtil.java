package br.com.desafio.app.security;

public class TokenIdUtil {

	private Long id;
	
	
	
	public TokenIdUtil() {
	}

	public TokenIdUtil(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TokenIdUtil [id=" + id + "]";
	}
}
