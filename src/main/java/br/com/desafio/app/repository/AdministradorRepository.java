package br.com.desafio.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.desafio.app.models.Administrador;

@Repository
public interface AdministradorRepository extends JpaRepository<Administrador, Long> {
	Administrador findByEmail(String email);
}
