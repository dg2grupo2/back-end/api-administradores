package br.com.desafio.app.DTO;

import br.com.desafio.app.models.Administrador;

public class AdministradorResDTO {

	private Long   id;
	private String nome;
	private String email;
	
	
	public AdministradorResDTO(Administrador adm) {
		this.id = adm.getId();
		this.nome = adm.getNome();
		this.email = adm.getEmail();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
}
