package br.com.desafio.app.DTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AdministradorReqDTO {

	@NotEmpty(message = "O campo nome é obrigatório.")
	@Size(min=3, max=100)
	private String nome;
	
	@NotEmpty(message = "O campo e-mail é obrigatório.")
	@Email
	private String email;
	
	@NotEmpty(message = "A senha deve conter pelo menos 4 caracteres.")
	@Size(min=4, max=100)
	private String senha;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public String toString() {
		return "AdministradorReqDTO [nome=" + nome + ", email=" + email + ", senha=" + senha + "]";
	}	
	
	
}
