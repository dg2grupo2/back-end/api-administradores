package br.com.desafio.app.DTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class AdministradorLoginDTO {

	@NotEmpty(message = "O campo e-mail é obrigatório.")
	@Email
	private String email;
	
	@NotEmpty(message = "O campo senha deve conter pelo menos 4 caracteres")
	private String senha;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
