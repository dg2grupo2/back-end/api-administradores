CREATE TABLE administradores (
  id BIGINT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(100) DEFAULT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  senha VARCHAR(255) NOT NULL,
  status VARCHAR(40) DEFAULT NULL,
  PRIMARY KEY (id)
);

INSERT INTO administradores ( nome, email, senha, status)
VALUES ( 'Lucas Machado', 'lucasm@hotmail.com', 'QgEcWqd7Ft7rrjdYGlPhqQ==', 'ativo'),
( 'Maria Luiza', 'marialu@hotmail.com', 'QgEcWqd7Ft7rrjdYGlPhqQ==', 'ativo'),
( 'Beatriz Figueira', 'belfigueira@hotmail.com', 'QgEcWqd7Ft7rrjdYGlPhqQ==', 'ativo');