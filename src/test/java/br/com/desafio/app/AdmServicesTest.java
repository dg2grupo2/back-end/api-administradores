package br.com.desafio.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;

import br.com.desafio.app.DTO.AdministradorLoginDTO;
import br.com.desafio.app.DTO.AdministradorReqDTO;
import br.com.desafio.app.DTO.AdministradorResDTO;
import br.com.desafio.app.models.Administrador;
import br.com.desafio.app.repository.AdministradorRepository;
import br.com.desafio.app.security.Token;
import br.com.desafio.app.security.TokenUtil;
import br.com.desafio.app.services.AdministradorServiceImp;

@ExtendWith(MockitoExtension.class)
class AdmServicesTest {
	
	private AdministradorServiceImp instance;
	
	@Captor
	ArgumentCaptor<Administrador> admReqCaptor;
	
	@Mock
	private AdministradorServiceImp mockInstance;
	
	@Mock
	private AdministradorRepository admRepository;
	
	@BeforeEach public void beforeEach() {
		MockitoAnnotations.openMocks(this);
		this.instance = new AdministradorServiceImp(admRepository);
		this.mockInstance = new AdministradorServiceImp(admRepository);
	}
	
	@Test
	void shouldReturnUserAuthenticated() {
	// arrange
		AdministradorLoginDTO login = administradorFakeLogin();
		Administrador adm = administradorFake();
		adm.setId(1L);
		
		TokenUtil verifyAccount = new TokenUtil(instance);
		when(admRepository.findByEmail(login.getEmail())).thenReturn(adm);
		doReturn(Optional.of(adm)).when(admRepository).findById(adm.getId());
		
	// actions		
		 Token token = createToken(login);
		 Authentication auth = verifyAccount.tokenValidator(token.getToken());
		 
	// assertions
		 assertTrue(auth.isAuthenticated());
		 verify(admRepository).findById(adm.getId());
	}
	
	@Test
	void shouldReturnToken() {
		// arrange
		AdministradorLoginDTO login = administradorFakeLogin();
		Administrador adm = administradorFake();
		adm.setId(1L);
		
		when(admRepository.findByEmail(login.getEmail())).thenReturn(adm);
		
		// actions		
		 Token token = createToken(login);

		// assertions
		assertThat(token).isNotNull();
		assertThat(token.getId()).isNotNull();
		assertThat(token.getToken()).isNotNull();
		verify(admRepository).findByEmail(login.getEmail());
	}
	
	@Test
	void shouldRegisterAdministrador() {
		// arrange
		Administrador newAdm = administradorFake();
		
		when(admRepository.save(admReqCaptor.capture())).thenAnswer(invocationOnMock -> {
			Administrador value = admReqCaptor.getValue();
			value.setId(1L);
			return value;
		});

		AdministradorReqDTO admReq = new AdministradorReqDTO();
		admReq.setEmail(newAdm.getEmail());
		admReq.setNome(newAdm.getNome());
		admReq.setSenha("Senha-Fake");
		
		// actions
		admRepository.save(newAdm);
		AdministradorResDTO admRes = instance.createAdministrador(admReq);
		
		// assertions
		assertThat(admRes).isNotNull();
		assertThat(admRes.getId()).isEqualTo(1);
		verify(admRepository).save(newAdm);
	}
	
	private Token createToken( AdministradorLoginDTO login) {
		try {
			return instance.generateAdmToken(login);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Administrador administradorFake() {
		Administrador adm = new Administrador();
		adm.setEmail("teste.case@teset.com");
		adm.setNome("Admin Fake");
		adm.setSenha("QgEcWqd7Ft7rrjdYGlPhqQ==");
		
		return adm;
	}
	
	private AdministradorLoginDTO administradorFakeLogin() {
		AdministradorLoginDTO admLogin = new AdministradorLoginDTO();
		admLogin.setEmail("teste.case@teset.com");
		admLogin.setSenha("Senha-Fake");
		
		return admLogin;
	}
}
