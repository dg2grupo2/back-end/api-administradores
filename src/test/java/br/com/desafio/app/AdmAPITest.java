package br.com.desafio.app;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.desafio.app.DTO.AdministradorLoginDTO;
import br.com.desafio.app.DTO.AdministradorReqDTO;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AdmAPITest {
	
	private static final String INVALID_TOKEN = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
	private static final String INEXISTENT_USER_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJBZG1pbmlzdHJhZG9yZXNfQXBwIiwidXNlcklkIjozOX0.JjCRPieG8dA-TTcF5_QU-Wt6Ai0fCvYhRjEWWRZUzEo";
	
	@Autowired
	private MockMvc mockMvc;
	
	
	@Test
	public void shouldReturnListOfUsers() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/administradores"))
			   .andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	void shouldReturnOkWhenCreateAdministrator() throws Exception {
		AdministradorReqDTO adm = administradorFake();
		String json = new ObjectMapper().writeValueAsString(adm);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/administradores/cadastrar")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated());
	}
	
	@Test
	void shouldReturnBadRequestWhenEmptyPasswordCreateAdministrator() throws Exception {
		AdministradorReqDTO adm = administradorFake();
		adm.setSenha("");
		String json = new ObjectMapper().writeValueAsString(adm);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/administradores/cadastrar")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	void shouldReturnBadRequestWhenInvalidEmailCreateAdministrator() throws Exception {
		AdministradorReqDTO adm = administradorFake();
		adm.setEmail("emailfake");
		String json = new ObjectMapper().writeValueAsString(adm);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/administradores/cadastrar")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	void shouldReturnOkWhenLoginIsValid() throws Exception {
		AdministradorLoginDTO admLogin = administradorFakeLogin();
		
		String json = new ObjectMapper().writeValueAsString(admLogin);
		mockMvc.perform(MockMvcRequestBuilders.post("/administradores/login")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	
	@Test
	void shouldReturnUnauthorizedWhenLoginIsInValid() throws Exception {
		AdministradorLoginDTO admLogin = administradorFakeLogin();
		admLogin.setSenha("Senha-Fa");
		
		String json = new ObjectMapper().writeValueAsString(admLogin);
		mockMvc.perform(MockMvcRequestBuilders.post("/administradores/login")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isUnauthorized());
	}

//	@Test
//	void shouldReturnOkWhenTokenIsValid() throws Exception {
//		System.out.println("*** Testando shouldReturnOkWhenTokenIsValid:");
//		mockMvc.perform(factoryBuilder("/validate", TOKEN))
//		.andExpect(status().isOk())
//		.andDo(print());
//	}
	
	@Test
	void shouldReturnForbiddenWhenUserIsInvalid() throws Exception {
		System.out.println("*** Testando shouldReturnForbiddenWhenUserIsInvalid:");
		mockMvc.perform(factoryBuilder("/validate", INEXISTENT_USER_TOKEN))
		.andExpect(status().isForbidden())
		.andDo(print());
	}
	
	@Test
	void shouldReturnForbiddenWhenTokenIsInvalidValid() throws Exception {
		System.out.println("*** Testando shouldReturnForbiddenWhenTokenIsInvalidValid:");
		mockMvc.perform(factoryBuilder("/validate", INVALID_TOKEN))
		.andExpect(status().isForbidden())
		.andDo(print());
	}
	
	private AdministradorReqDTO administradorFake() {
		AdministradorReqDTO adm = new AdministradorReqDTO();
		adm.setEmail("teste.case@teset.com");
		adm.setNome("Admin Fake");
		adm.setSenha("Senha-Fake");
		
		return adm;
	}
	
	private AdministradorLoginDTO administradorFakeLogin() {
		AdministradorLoginDTO admLogin = new AdministradorLoginDTO();
		admLogin.setEmail("lucasm@hotmail.com");
		admLogin.setSenha("Senha-Fake");
		
		return admLogin;
	}
	
	private MockHttpServletRequestBuilder factoryBuilder(String url, String token) {
		return MockMvcRequestBuilders.post(url)
				.header("Authorization", token.trim());
	}
}
